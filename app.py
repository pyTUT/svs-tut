# coding=utf-8
from __future__ import with_statement

import os
import shutil
import time

from flask import Flask, request, render_template, send_from_directory
from pydub import AudioSegment
from werkzeug.utils import secure_filename

from DoExperiment import split_it

DATA_DIR = 'static/data_dir'
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = DATA_DIR


def save_upload_files(file, now_time_str):
    upload_file_path = "nothing"
    if file:
        filename = file.filename
        filename = now_time_str + '/' + filename
        filename = secure_filename(filename)
        upload_file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        if not os.path.isdir(os.path.dirname(upload_file_path)):
            os.makedirs(os.path.dirname(upload_file_path))
        file.save(upload_file_path)
    return upload_file_path


def convert_mp3_wav(wav_path):
    ext_format = os.path.splitext(wav_path)[-1]
    if '.wav' != wav_path.lower():
        sound = AudioSegment.from_file(wav_path, ext_format[1:])
        sound = sound.set_channels(1)
        sound = sound.set_frame_rate(16000)
        sound.export(wav_path.replace(ext_format, '.wav'), 'wav')
    return wav_path.replace(ext_format, '.wav')


@app.route('/res/<path:filename>', methods=['GET', 'POST'])
def download(filename):
    uploads = './static/data_dir'
    return send_from_directory(directory=uploads, filename=filename)


@app.route('/', methods=['GET', 'POST'])
def index():
    info = {'v': '',
            'a': '',
            'h': 'hidden'}
    if request.method == 'POST':
        shutil.rmtree(DATA_DIR, ignore_errors=True)
        if not os.path.isdir(DATA_DIR):
            os.makedirs(DATA_DIR)
        AudioFile = request.files['AudioFile']
        now_time_str = time.strftime('%Y%m%d_%H%M%S', time.localtime(time.time()))
        audio = save_upload_files(AudioFile, now_time_str)
        audio = convert_mp3_wav(audio)
        v,a=split_it(audio)
        info['v'] = '/%s' % v
        info['a'] = '/%s' % a
        info['h'] = ''
        return render_template('index.html', info=info)
    return render_template('index.html', info=info)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8999, debug=False)  # accept external post
