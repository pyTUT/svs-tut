#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
from pydub import AudioSegment

import util



def convert_wav_mp3(wav_path):
    song = AudioSegment.from_file(wav_path, 'wav')
    song.export(wav_path.replace('.wav', '.mp3'), 'mp3')
    return wav_path.replace('.wav', '.mp3')


def use_model_get_VA(mag, start, end):
    mag=np.float32(mag)
    mask = util.ComputeMask(mag[:, start:end], unet_model="unet.model", hard=False)
    V = mag[:, start:end] * mask
    A = mag[:, start:end] * (1 - mask)
    return V, A


def split_it(fname):
    mag, phase, dur = util.LoadAudio(fname)

    start = 0
    end = mag.shape[-1] // 1024 * 1024
    V = 'V'
    A ='A'
    for item in range(start, end, 1024):
        if type(V) == str and type(A)==str:
            V, A = use_model_get_VA(mag, item, item + 1024)
        else:
            Vv, Aa = use_model_get_VA(mag, item, item + 1024)
            V = np.hstack((V, Vv))
            A = np.hstack((A, Aa))

    util.SaveAudio(
        "%s-V.wav" % fname.strip('.wav'), V, phase[:, start:end], dur)
    util.SaveAudio(
        "%s-A.wav" % fname.strip('.wav'), A, phase[:, start:end], dur)
    try:
        v_path=convert_wav_mp3("%s-V.wav" % fname.strip('.wav'))
        a_path=convert_wav_mp3("%s-A.wav" % fname.strip('.wav'))
    except:
        v_path="%s-V.wav" % fname.strip('.wav')
        a_path="%s-A.wav" % fname.strip('.wav')

    return v_path,a_path

if __name__ == '__main__':
    split_it('data_dir/20201005_164748_lsadm.mp3')
    